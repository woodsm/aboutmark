<!-- @format -->

[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://github.com/Markieefreshh/markiefreshh)
![Node.js Build and Deploy](https://github.com/Markieefreshh/aboutmark/workflows/Node.js%20Build%20and%20Deploy/badge.svg)

# djmarkieefreshh

[DJ Markiee Freshh Official Website](https://djmarkieefreshh.com/)

## Authors

- **Matthew Woods** - _Developer_ - [Matthew](https://github.com/mxttwoods)
- **Mark Machizzga** - _Designer_ - [Mark](https://github.com/Markieefreshh)

## License

This project is licensed under the MIT License - see the LICENSE.md file for
details
