const htmlmin = require('html-minifier')
module.exports = function (eleventyConfig) {
	eleventyConfig.addPassthroughCopy('src/assets')
	eleventyConfig.addTransform('htmlmin', function (content, outputPath) {
		if (outputPath.endsWith('.html')) {
			let minified = htmlmin.minify(content, {
				minifyCSS: true,
				removeTagWhitespace: true,
				minifyJS: true,
				useShortDoctype: true,
				removeComments: true,
				collapseWhitespace: true
			})
			return minified
		}
		return content
	})
	return {
		dir: {
			input: 'src',
			output: '_site'
		}
	}
}
